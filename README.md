
**Test WPF Project**
-
This program for analysis user's daily activity.

Main features:
 - List with user's ordered by average activity.
 - Daily activity chart for selected user.
 - Import/Export records with JSON format.

Imported file structure:

    [{
	  "Rank": 1,
	  "User": "Липатов Александр",
	  "Status": "Finished",
	  "Steps": 66683
	},
	{
      "Rank": 2,
      "User": "Косинов Павел",
      "Status": "Finished",
      "Steps": 41389
    }]

Exported file structure:

    {
      "User": "Малкин Владимир",
      "Rank": 97,
      "AverageActivity": 1749,
      "BestActivity": 2113,
      "WorstActivity": 1431,
      "DayRecords": [
        {
          "Day": 1,
          "Status": "Finished",
          "Steps": 1524,
          "Rank": 95
        },
        {
          "Day": 2,
          "Status": "Finished",
          "Steps": 1962,
          "Rank": 94
        }
      ]
    }
Screenshots:
![](activity-analyzer.png)

Dependecies and references:
 - C# 8.0.
 - .NET Framework 4.8.
 - LiveCharts 0.9.7.
 - LiveCharts.Wpf 0.9.7.
 - StyleCop.Analyzers 1.1.118.

