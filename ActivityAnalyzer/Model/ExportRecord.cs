﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Object for contract serialization.
    /// </summary>
    [DataContract]
    public class ExportRecord
    {
        /// <summary>
        /// Gets or sets user's name.
        /// </summary>
        /// <value>User name.</value>
        [DataMember(Order = 1)]
        public string User { get; set; }

        /// <summary>
        /// Gets or sets user's rank.
        /// </summary>
        /// <value>User's rank.</value>
        [DataMember(Order = 2)]
        public int Rank { get; set; }

        /// <summary>
        /// Gets or sets user's average number of steps in specified period.
        /// </summary>
        /// <value>Average number of steps.</value>
        [DataMember(Order = 3)]
        public int AverageActivity { get; set; }

        /// <summary>
        /// Gets or sets user's best number of steps in specified period.
        /// </summary>
        /// <value>Best number of steps.</value>
        [DataMember(Order = 4)]
        public int BestActivity { get; set; }

        /// <summary>
        /// Gets or sets user's worst number of steps in specified period.
        /// </summary>
        /// <value>Worst number of steps.</value>
        [DataMember(Order = 5)]
        public int WorstActivity { get; set; }

        /// <summary>
        /// Gets or sets list of user's records.
        /// </summary>
        /// <value>List of user's records.</value>
        [DataMember(Order = 6)]
        public List<ExportDayRecord> DayRecords { get; set; } = new List<ExportDayRecord>();
    }
}
