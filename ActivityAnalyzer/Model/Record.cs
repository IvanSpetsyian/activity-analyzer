﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Contains informaiton about user activity. Implements <see cref="INotifyPropertyChanged"/>  interface.
    /// </summary>
    public class Record : INotifyPropertyChanged
    {
        private string user = string.Empty;
        private string status = string.Empty;
        private int rank;
        private int steps;
        private int averageActivity;
        private int bestActivity;
        private int worstActivity;
        private short day;
        private Progress progress;

        /// <inheritdoc/>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets user's name. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>User name.</value>
        public string User
        {
            get
            {
                return this.user;
            }

            set
            {
                this.user = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's activity day status. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Activity status.</value>
        public string Status
        {
            get
            {
                return this.status;
            }

            set
            {
                this.status = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's rank. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>User's rank.</value>
        public int Rank
        {
            get
            {
                return this.rank;
            }

            set
            {
                this.rank = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's number of steps in <see cref="Day"/>. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Number of steps.</value>
        public int Steps
        {
            get
            {
                return this.steps;
            }

            set
            {
                this.steps = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's average number of steps in specified period. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Average number of steps.</value>
        public int AverageActivity
        {
            get
            {
                return this.averageActivity;
            }

            set
            {
                this.averageActivity = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's best number of steps in specified period. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Best number of steps.</value>
        public int BestActivity
        {
            get
            {
                return this.bestActivity;
            }

            set
            {
                this.bestActivity = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's worst number of steps in specified period. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Worst number of steps.</value>
        public int WorstActivity
        {
            get
            {
                return this.worstActivity;
            }

            set
            {
                this.worstActivity = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets record's creating day. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Record's creating day.</value>
        public short Day
        {
            get
            {
                return this.day;
            }

            set
            {
                this.day = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets user's progress. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>User's progress.</value>
        public Progress Progress
        {
            get
            {
                return this.progress;
            }

            set
            {
                this.progress = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Called when specified property was changed. Calling <see cref="PropertyChangedEventArgs"/> with caller class and caller property name.
        /// </summary>
        /// <param name="property">Caller property name. Empty string by default.</param>
        public void OnPropertyChanged([CallerMemberName] string property = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
