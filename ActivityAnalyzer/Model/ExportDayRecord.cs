﻿using System.Runtime.Serialization;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Object for contract serialization.
    /// </summary>
    [DataContract]
    public class ExportDayRecord
    {
        /// <summary>
        /// Gets or sets record's creating day.
        /// </summary>
        /// <value>Record's creating day.</value>
        [DataMember(Order = 1)]
        public int Day { get; set; }

        /// <summary>
        /// Gets or sets user's number of steps in <see cref="Day"/>.
        /// </summary>
        /// <value>Number of steps.</value>
        [DataMember(Order = 2)]
        public int Steps { get; set; }

        /// <summary>
        /// Gets or sets user's activity day status.
        /// </summary>
        /// <value>Activity status.</value>
        [DataMember(Order = 3)]
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets user's rank in current day.
        /// </summary>
        /// <value>User's rank.</value>
        [DataMember(Order = 4)]
        public int Rank { get; set; }
    }
}
