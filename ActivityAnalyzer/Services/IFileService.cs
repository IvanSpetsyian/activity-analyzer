﻿using System.Collections.ObjectModel;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Provides functionaluty for work with file services.
    /// </summary>
    public interface IFileService
    {
        /// <summary>
        /// Provides functionaluty for files import.
        /// </summary>
        /// <param name="fileNames">Paths to files for import.</param>
        /// <param name="recordsDataService">Object of <paramref name="recordsDataService"/> type for import.</param>
        /// <returns>List of imported <see cref="Record"/>.</returns>
        ReadOnlyCollection<Record> Import(string[] fileNames, RecordsDataService recordsDataService);

        /// <summary>
        /// Provides functionaluty for file export.
        /// </summary>
        /// <param name="filename">Specified file name.</param>
        /// <param name="record">Record for export.</param>
        /// <param name="recordsDataService">Object of <paramref name="recordsDataService"/> type with daily records for export.</param>
        void Export(string filename, Record record, RecordsDataService recordsDataService);
    }
}
