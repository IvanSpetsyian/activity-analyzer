﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Provides methods for manipulation with records.
    /// </summary>
    public class RecordsDataService
    {
        private static readonly double DifferenceBetterPercent = 1.2;
        private static readonly double DifferenceWorsePercent = 0.8;
        private readonly Dictionary<string, List<Record>> usersDictionary = new Dictionary<string, List<Record>>(StringComparer.InvariantCultureIgnoreCase);

        /// <summary>
        /// Add data to dictionary categorized by user name.
        /// </summary>
        /// <param name="day">Record creating day.</param>
        /// <param name="records">User records for specified day.</param>
        public void AddDayData(short day, IEnumerable<Record> records)
        {
            foreach (var record in records)
            {
                record.Day = day;

                if (this.usersDictionary.ContainsKey(record.User))
                {
                    this.usersDictionary[record.User].Add(record);
                }
                else
                {
                    this.usersDictionary.Add(record.User, new List<Record>() { record });
                }
            }
        }

        /// <summary>
        /// Sets users rank and gets list ordered by rank.
        /// </summary>
        /// <returns>Returns users list ordered by rank.</returns>
        public ReadOnlyCollection<Record> GetRankedList()
        {
            var records = this.GetUsersList();
            records.Sort((a, b) => b.AverageActivity.CompareTo(a.AverageActivity));

            for (int i = 0; i < records.Count; i++)
            {
                records[i].Rank = i + 1;
            }

            return records.AsReadOnly();
        }

        /// <summary>
        /// Gets list of <see cref="Record"/> ordered by creating day for <paramref name="userName"/>.
        /// </summary>
        /// <param name="userName">Specified user's name.</param>
        /// <returns>Returns list of <see cref="Record"/>.</returns>
        public ReadOnlyCollection<Record> GetDailyRecords(string userName)
        {
            if (this.usersDictionary.TryGetValue(userName, out List<Record> records))
            {
                return records.AsReadOnly();
            }

            return records.AsReadOnly();
        }

        /// <summary>
        /// Choose user progress base on <paramref name="averageActivity"/>, <paramref name="bestActivity"/>, <paramref name="worstActivity"/>.
        /// </summary>
        /// <param name="averageActivity">User's average number of steps.</param>
        /// <param name="bestActivity">User's best number of steps.</param>
        /// <param name="worstActivity">User's worst number of steps.</param>
        /// <returns>Returns user's <see cref="Progress"/>.</returns>
        public Progress UserProgress(int averageActivity, int bestActivity, int worstActivity) => averageActivity switch
        {
            int _ when bestActivity > averageActivity * DifferenceBetterPercent => Progress.Improve,
            int _ when worstActivity < averageActivity * DifferenceWorsePercent => Progress.Worsen,
            _ => Progress.Normally,
        };

        /// <summary>
        /// Delete all recocords in current <see cref="RecordsDataService"/>.
        /// </summary>
        public void ClearService() => this.usersDictionary.Clear();

        /// <summary>
        /// Gets usorted users list.
        /// </summary>
        /// <returns>Returns unsorted users list.</returns>
        private List<Record> GetUsersList()
        {
            var records = new List<Record>();

            foreach (var userName in this.usersDictionary.Select(user => user.Key))
            {
                var record = new Record()
                {
                    User = userName,
                    AverageActivity = this.GetAverageUserActivity(userName),
                    BestActivity = this.GetBestUserActivity(userName),
                    WorstActivity = this.GetWorstUserActivity(userName),
                };
                record.Progress = this.UserProgress(record.AverageActivity, record.BestActivity, record.WorstActivity);
                records.Add(record);
            }

            return records;
        }

        /// <summary>
        /// Gets <paramref name="userName"/> average number of steps.
        /// </summary>
        /// <param name="userName">Specified user's name.</param>
        /// <returns>Returns average number of steps.</returns>
        private int GetAverageUserActivity(string userName)
        {
            int summarySteps = 0;
            int averageSteps = 0;

            if (this.usersDictionary.TryGetValue(userName, out List<Record> records))
            {
                foreach (var record in records)
                {
                    summarySteps += record.Steps;
                }

                averageSteps = summarySteps / records.Count;
            }

            return averageSteps;
        }

        /// <summary>
        /// Gets <paramref name="userName"/> best number of steps.
        /// </summary>
        /// <param name="userName">Specified user's name.</param>
        /// <returns>Returns best number of steps.</returns>
        private int GetBestUserActivity(string userName)
        {
            int maximumSteps = 0;

            if (this.usersDictionary.TryGetValue(userName, out List<Record> records))
            {
                foreach (var record in records)
                {
                    maximumSteps = Math.Max(maximumSteps, record.Steps);
                }
            }

            return maximumSteps;
        }

        /// <summary>
        /// Gets <paramref name="userName"/> worst number of steps.
        /// </summary>
        /// <param name="userName">Specified user's name.</param>
        /// <returns>Returns worst number of steps.</returns>
        private int GetWorstUserActivity(string userName)
        {
            int minimumSteps = int.MaxValue;

            if (this.usersDictionary.TryGetValue(userName, out List<Record> records))
            {
                foreach (var record in records)
                {
                    minimumSteps = Math.Min(minimumSteps, record.Steps);
                }
            }

            return minimumSteps;
        }
    }
}
