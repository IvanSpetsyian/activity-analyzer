﻿namespace ActivityAnalyzer
{
    /// <summary>
    /// Provides progress values.
    /// </summary>
    public enum Progress
    {
        /// <summary>
        /// User's progress improve.
        /// </summary>
        Improve,

        /// <summary>
        /// User's progress haven't significant changes.
        /// </summary>
        Normally,

        /// <summary>
        /// User's progress worsen.
        /// </summary>
        Worsen,
    }
}
