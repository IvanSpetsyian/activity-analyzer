﻿using System.Windows.Forms;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Default implementation of <see cref="IDialogService"/>.
    /// </summary>
    public class DefaultDialogService : IDialogService
    {
        /// <inheritdoc/>
        public string FilePath { get; set; }

        /// <inheritdoc/>
        public string[] FileNames { get; set; }

        /// <inheritdoc/>
        public bool ImportFilesDialog()
        {
            var importFilesDialog = new OpenFileDialog { Multiselect = true };
            var dr = importFilesDialog.ShowDialog();

            if (dr == DialogResult.OK)
            {
                this.FileNames = importFilesDialog.FileNames;
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool ExportFileDialog()
        {
            var exportFileDialog = new SaveFileDialog();
            if (exportFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.FilePath = exportFileDialog.FileName;
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}