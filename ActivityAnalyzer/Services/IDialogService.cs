﻿namespace ActivityAnalyzer
{
    /// <summary>
    /// Provides functionaluty for work with dialog services.
    /// </summary>
    public interface IDialogService
    {
        /// <summary>
        /// Gets or sets path to file.
        /// </summary>
        /// <value>Path to file.</value>
        string FilePath { get; set; }

        /// <summary>
        /// Gets or sets paths to files.
        /// </summary>
        /// <value>Array of strings with paths to files.</value>
        public string[] FileNames { get; set; }

        /// <summary>
        /// Show message box with specified <paramref name="message"/>.
        /// </summary>
        /// <param name="message">Specified message.</param>
        void ShowMessage(string message);

        /// <summary>
        /// Provides dialog for import.
        /// </summary>
        /// <returns>Returns true if files was set to <see cref="FileNames"/>, false otherwise.</returns>
        bool ImportFilesDialog();

        /// <summary>
        /// Provides dialog for export.
        /// </summary>
        /// <returns>Returns true if file was set to <see cref="FilePath"/>, false otherwise.</returns>
        bool ExportFileDialog();
    }
}
