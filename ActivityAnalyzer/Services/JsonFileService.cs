﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;

namespace ActivityAnalyzer
{
    /// <summary>
    /// Implement <see cref="IFileService"/> for JSON format.
    /// </summary>
    public class JsonFileService : IFileService
    {
        /// <inheritdoc/>
        public ReadOnlyCollection<Record> Import(string[] fileNames, RecordsDataService recordsDataService)
        {
            if (fileNames == null)
            {
                throw new ArgumentNullException(nameof(fileNames), "No files to import.");
            }

            recordsDataService.ClearService();
            var jsonFormatter = new DataContractJsonSerializer(typeof(List<Record>));

            foreach (var file in fileNames)
            {
                var day = Convert.ToInt16(file.Split('\\').Last().Substring(3, Path.GetFileName(file).Length - 8));
#pragma warning disable IDE0063
                using (var fs = new FileStream(file, FileMode.OpenOrCreate))
                {
                    var records = jsonFormatter.ReadObject(fs) as List<Record>;
                    recordsDataService.AddDayData(day, records);
                }

#pragma warning restore IDE0063
            }

            return recordsDataService.GetRankedList();
        }

        /// <inheritdoc/>
        public void Export(string filename, Record record, RecordsDataService recordsDataService)
        {
            var contractRecord = this.CreateContractRecord(record, recordsDataService);

            var jsonFormatter = new DataContractJsonSerializer(typeof(ExportRecord));
#pragma warning disable IDE0063
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                var encoding = System.Text.Encoding.UTF8;
                var ownsStream = false;
                var indent = true;

                using (var writer = JsonReaderWriterFactory.CreateJsonWriter(fs, encoding, ownsStream, indent))
                {
                    jsonFormatter.WriteObject(writer, contractRecord);
                }
            }
#pragma warning restore IDE0063
        }

        /// <summary>
        /// Create record for serialization.
        /// </summary>
        /// <param name="record">User record with general data.</param>
        /// <param name="recordsDataService">Object of <paramref name="recordsDataService"/> type with daily records.</param>
        /// <returns>Record for serialization.</returns>
        private ExportRecord CreateContractRecord(Record record, RecordsDataService recordsDataService)
        {
            var contractRecord = new ExportRecord()
            {
                User = record.User,
                Rank = record.Rank,
                AverageActivity = record.AverageActivity,
                BestActivity = record.BestActivity,
                WorstActivity = record.WorstActivity,
            };

            foreach (var rec in recordsDataService.GetDailyRecords(record.User))
            {
                contractRecord.DayRecords.Add(new ExportDayRecord()
                {
                    Day = rec.Day,
                    Status = rec.Status,
                    Steps = rec.Steps,
                    Rank = rec.Rank,
                });
            }

            return contractRecord;
        }
    }
}