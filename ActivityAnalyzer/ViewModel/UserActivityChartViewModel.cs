﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;

namespace ActivityAnalyzer
{
    /// <summary>
    /// ViewModel for user's activity chart.
    /// </summary>
    public class UserActivityChartViewModel : INotifyPropertyChanged
    {
        private SeriesCollection seriesCollection;
        private CartesianMapper<int> mapper;
        private List<string> labels;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserActivityChartViewModel"/> class.
        /// </summary>
        public UserActivityChartViewModel()
        {
            this.SeriesCollection = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Steps",
                    Values = this.Values,
                },
            };
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets or sets <see cref="LiveCharts.SeriesCollection"/>. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value><see cref="LiveCharts.SeriesCollection"/>.</value>
        public SeriesCollection SeriesCollection
        {
            get
            {
                return this.seriesCollection;
            }

            set
            {
                this.seriesCollection = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets <see cref="ChartValues{T}"/>.
        /// </summary>
        /// <value>Values for chart.</value>
        public ChartValues<int> Values { get; set; } = new ChartValues<int>();

        /// <summary>
        /// Gets or sets <see cref="CartesianMapper{T}"/>.
        /// </summary>
        /// <value>Mapper for chart.</value>
        public CartesianMapper<int> Mapper
        {
            get
            {
                return this.mapper;
            }

            set
            {
                this.mapper = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets formatter for Y-axis.
        /// </summary>
        /// <value>Formatter.</value>
        public Func<double, string> YFormatter { get; set; } = val => ((int)val).ToString();

        /// <summary>
        /// Gets or sets labels for X-axis. Called <see cref="OnPropertyChanged"/> if sets.
        /// </summary>
        /// <value>Labels for X-axis.</value>
        public List<string> Labels
        {
            get
            {
                return this.labels;
            }

            set
            {
                this.labels = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets <see cref="Brush"/> for best activity.
        /// </summary>
        /// <value>Brush for best activity.</value>
        public Brush BestBrush { get; set; } = new SolidColorBrush(Color.FromRgb(119, 167, 119));

        /// <summary>
        /// Gets or sets <see cref="Brush"/> for worst activity.
        /// </summary>
        /// <value>Brush for worst activity.</value>
        public Brush WorstBrush { get; set; } = new SolidColorBrush(Color.FromRgb(241, 70, 70));

        /// <summary>
        /// Updates chart with specified user's data.
        /// </summary>
        /// <param name="recordsDataService">Service with <see cref="RecordsDataService"/> type. Called <see cref="OnPropertyChanged"/> if sets.</param>
        /// <param name="selectedRecord">Selected record.</param>
        public void UpdateChart(RecordsDataService recordsDataService, Record selectedRecord)
        {
            this.Values.Clear();
            this.Labels = new List<string>();
            this.Mapper = Mappers.Xy<int>()
                .X((item, index) => index)
                .Y(item => item)
                .Fill(item =>
                {
                    return item switch
                    {
                        int _ when item == selectedRecord.BestActivity => this.BestBrush,
                        int _ when item == selectedRecord.WorstActivity => this.WorstBrush,
                        _ => null,
                    };
                });

            foreach (var rec in recordsDataService.GetDailyRecords(selectedRecord.User))
            {
                this.Values.Add(rec.Steps);
                this.Labels.Add(rec.Day.ToString());
            }
        }

        /// <summary>
        /// Called when specified property was changed. Calling <see cref="PropertyChangedEventArgs"/> with caller class and caller property name.
        /// </summary>
        /// <param name="property">Caller property name. Empty string by default.</param>
        public void OnPropertyChanged([CallerMemberName] string property = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
