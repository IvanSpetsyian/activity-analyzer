﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;

namespace ActivityAnalyzer
{
    /// <summary>
    /// ViewModel for user's records list.
    /// </summary>
    public class RecordsListViewModel : INotifyPropertyChanged
    {
        private readonly IFileService fileService;
        private readonly IDialogService dialogService;
        private readonly RecordsDataService recordsDataService;
        private readonly UserActivityChartViewModel userActivityChartViewModel;
        private ObservableCollection<Record> records;
        private Record selectedRecord;
        private RelayCommand updateChartCommand;
        private RelayCommand importCommand;
        private RelayCommand exportCommand;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecordsListViewModel"/> class.
        /// </summary>
        /// <param name="dialogService">Set dialog service.</param>
        /// <param name="fileService">Set file service.</param>
        public RecordsListViewModel(IDialogService dialogService, IFileService fileService)
        {
            this.fileService = fileService;
            this.dialogService = dialogService;
            this.recordsDataService = new RecordsDataService();
            this.userActivityChartViewModel = new UserActivityChartViewModel();
        }

        /// <inheritdoc/>
        public event PropertyChangedEventHandler PropertyChanged;

#pragma warning disable SA1609, SA1623
        /// <summary>
        /// Command for chart updating.
        /// </summary>
        public RelayCommand UpdateChartCommand
        {
            get
            {
                return this.updateChartCommand ??= new RelayCommand(obj =>
                {
                    if (this.selectedRecord != null)
                    {
                        this.userActivityChartViewModel.UpdateChart(this.recordsDataService, this.SelectedRecord);
                    }
                });
            }
        }

        /// <summary>
        /// Command for importing files.
        /// </summary>
        public RelayCommand ImportCommand
        {
            get
            {
                return this.importCommand ??= new RelayCommand(obj =>
                {
                    try
                    {
                        if (this.dialogService.ImportFilesDialog())
                        {
                            this.Records = new ObservableCollection<Record>(this.fileService.Import(
                                                                                                    this.dialogService.FileNames,
                                                                                                    this.recordsDataService));
                            this.SelectedRecord = this.Records.FirstOrDefault();
                            this.dialogService.ShowMessage("File(s) was imported.");
                        }
                    }
                    catch (Exception ex)
                    {
                        this.dialogService.ShowMessage(ex.Message);
                    }
                });
            }
        }

        /// <summary>
        /// Command for exporting file.
        /// </summary>
        public RelayCommand ExportCommand
        {
            get
            {
                return this.exportCommand ??= new RelayCommand(obj =>
                {
                    try
                    {
                        if (this.dialogService.ExportFileDialog())
                        {
                            this.fileService.Export(this.dialogService.FilePath, this.SelectedRecord, this.recordsDataService);
                            this.dialogService.ShowMessage("File was exported.");
                        }
                    }
                    catch (Exception ex)
                    {
                        this.dialogService.ShowMessage(ex.Message);
                    }
                });
            }
        }
#pragma warning restore SA1609, SA1623

        /// <summary>
        /// Gets or sets list of records for <see cref="RecordsListViewModel"/>.
        /// </summary>
        /// <value><see cref="ObservableCollection{T}"/> list of records.</value>
        public ObservableCollection<Record> Records
        {
            get
            {
                return this.records;
            }

            set
            {
                this.records = value;
                this.OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets current selected record. Called <see cref="OnPropertyChanged"/> if sets. Called <see cref="UpdateChartCommand"/>.
        /// </summary>
        /// <value>Selected record.</value>
        public Record SelectedRecord
        {
            get
            {
                return this.selectedRecord;
            }

            set
            {
                this.selectedRecord = value;
                this.OnPropertyChanged();
                this.UpdateChartCommand.Execute(null);
            }
        }

        /// <summary>
        /// Gets <see cref="ActivityAnalyzer.UserActivityChartViewModel"/> which used with current <see cref="RecordsListViewModel"/>.
        /// </summary>
        /// <value><see cref="ActivityAnalyzer.UserActivityChartViewModel"/>.</value>
        public UserActivityChartViewModel UserActivityChartViewModel
        {
            get
            {
                return this.userActivityChartViewModel;
            }
        }

        /// <summary>
        /// Called when specified property was changed. Calling <see cref="PropertyChangedEventArgs"/> with caller class and caller property name.
        /// </summary>
        /// <param name="property">Caller property name. Empty string by default.</param>
        public void OnPropertyChanged([CallerMemberName] string property = "")
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
    }
}
